// Class pc (peerConnection).
//It manage the connection P2P between 2 browsers 

function pc (id,state,localStream){
	
	var calleeId = id; 			//String, stores the id of the callee
	var remoteVideo = null 		//Reference to the remote video element in the DOM
	var calleeReady = false; 	//When true, the callee has acepted the call
	var waitTime = 30;			//Int Time(secs) waiting for answer before auto hang up 
	var peerConnection = null;	//Stores the peerConnection object
	var remoteStream = null;	//Remote video stream
	var mediaConstraints = {'mandatory': {'OfferToReceiveAudio':true, 'OfferToReceiveVideo':true}}; 
	var mozMediaConstrains = {'mandatory': {'OfferToReceiveAudio':true, 'OfferToReceiveVideo':true, 'MozDontOfferDataChannel':true}}
	//Force to the SDP to send and receive video and audio available codecs information 
	var sendChannel = null;


//Public functions	
this.hangUp = function(){
	//peerConnection.removeStream(remoteStream.stream);
	console.log("Hanging up");
	//Removing subscriptions! 
	iosocket.removeListener("sdp",recSdp);
	iosocket.removeListener("candidate",recCandidate);
	if (peerConnection) {
		peerConnection.close();
		peerConnection = null;
	}	
}

this.getRemoteStream = function(){
	
	return remoteStream;
}

this.getCalleeReady = function(){
	return calleeReady;
}

this.getpc = function(){
	return peerConnection;
}

this.setCalleeReady = function(status){
  	calleeReady = status;
 }


//Private functions
//startPeerConnection function starts a peerConnection process
//#isCaller Boolean If true means that the client is starting a call, if false is receiving the call
  function startPeerConnection(isCaller){
  	remoteVideo = $("#"+calleeId+" video");
  	console.log("CalleeId is: " + calleeId);
  	if (userMedia){	
		  	if (isCaller)
		  		iosocket.emit("peer",calleeId);
		  	createPeerConnection(isCaller); 
		  	console.log("PeerConnection Started and Sent");
		  }else{
		  	console.log("colgando");
		  	iosocket.emit("hangup",calleeId);
		  }	
  }

	
// successSDP sets the local SDP.
  function successSDP(sdp){
  	console.log("Este es el SDP, y ahora lo envio");
  	console.log(sdp);

  	peerConnection.setLocalDescription(sdp); 
    iosocket.emit("sdp",calleeId,sdp);

  	
  }
  function failSDP(error){
  	console.log(error);
  	
  }

 
  // peerConnection implementation: Standard http://www.w3.org/TR/webrtc/
  // createPeerConnection define the PeerConnection and its listeners, it also manage the
  // logic of a new call (If it is caller, wait untill callee is ready, if it is callee, send
  // ready message to the caller).
  function createPeerConnection(isCaller) {

  	console.log("Creating PeerConnection");
  	try {
  		console.log("peerconnection before calling");
  		console.log(peerConnection);
		//peerConnection = new webkitRTCPeerConnection({iceServers:[{url:'stun:stun.fwdnet.net'}]},{optional: [{RtpDataChannels: true}]});
  		//INTEROPERABILITY CODE
  		if navigator.webkitRTCPeerConnection{
  			peerConnection = new webkitRTCPeerConnection({iceServers:[{url:'stun:stun.fwdnet.net'}]},{optional:[{'DtlsSrtpKeyAgreement': 'true'}]});
  		}else if navigator.mozRTCPeerConnection{
			peerConnection = new mozRTCPeerConnection({iceServers:[{url:'stun:stun.fwdnet.net'}]});
  		}else{
  			peerConnection = new RTCPeerConnection({iceServers:[{url:'stun:stun.fwdnet.net'}]});
  		}
  		console.log("RTCPeerConection established");
  		console.log(peerConnection);


  		// Adding listeners
  		peerConnection.onicechange = function (event) {
  			console.log("icestate has changed");
 		}
  		
		//defining onicecandidate
  		peerConnection.onicecandidate = function (event) {
  			if (event.candidate){
       // 	iosocket.emit("candidate",calleeId,{type: 'candidate',
       //            label: event.candidate.sdpMLineIndex,
         //          id: event.candidate.sdpMid,
           //        candidate: event.candidate.candidate});
            iosocket.emit("candidate",calleeId,event.candidate);
            }else{
            	console.log("End of candidates");
            	
            	//sAlert("The candidate were not found, try to recall again");
            	//removeFromConference(calleeId);
            }     
            
   		};
   	
   		peerConnection.onaddstream = function (event) {
   			console.log("I'm peerconnection adding stream of "+calleeId);
   			remoteStream = event;
   			remoteVideo.attr("src",URL.createObjectURL(remoteStream.stream));   			
   		}
   		
   		peerConnection.onstatechange = function (event) {
   			if (peerConnection){
   
	   			if (peerConnection.readyState === "closed" ){
	   				console.log("Conference is over");
	   			}		
   			}else{
   				console.log("peerConnection is already deleted.")
   				console.log(busy)
   			}	
   		}
   		peerConnection.onremovestream = function (event) {
   			if (peerConnection.readyState === "closed" ){
   				console.log("Steam is REMOVED");
 			
   			}	
   		}
   		
   		peerConnection.onclose = function (event){

   			$("#remote1").css("visiblility","hidden");
   			console.log("Conference is closed");   			
   		}

   		peerConnection.ondatachannel = receiveChannelCallback;
 		// End Adding listeners
 		
 		
 		
 		
 		// Managing connection with peer
 		
 		peerConnection.addStream(localStream);
 		
		if(isCaller){
		//	modal.calling(calleeId);
			WaitCalleeReady(0);
		}else{
			//If it's the Callee, fire the Ringing
		//	modal.ring(calleeId);
			iosocket.emit("ready",calleeId);
		//	offerCall(0);				
		}
		
		//make a funct
		
		/*
		function offerCall(i){
			
			if (i === null)
				i = 0;
			if (answer){
				
				
				console.log(peerConnection);				
				showConference();
			}else if ((i<waitTime) && peerConnection){
				i++;
				console.log("Ringing");
				setTimeout(function(){
					offerCall(i)
					},1000);
			
				}else{
					
					console.log("colgando");
					modal.stopRing();
					
					
				}
				
		}
		
		*/

		//Recursive function which wait for the callee's ready signal
		function WaitCalleeReady(i){
			if (i === null)
				i = 0;
			if (calleeReady){
				console.log("Creating offer");
				// INTEROPERABILITY
				if (window.webkitRTCPeerConnection){
					peerConnection.createOffer(successSDP,failSDP,mediaConstraints);
				}
				else{
					peerConnection.createOffer(successSDP,failSDP,mozMediaConstraints);
				}
				console.log("CalleeReady is "+calleeReady+" when hide is fired")
			}else if ((i<waitTime) && peerConnection ){
				i++;
				console.log("Waiting Callee Ready");
				setTimeout(function(){
					WaitCalleeReady(i)
					},1000);
			
				}else{
					sAlert(getNickname(id)+" is busy or doesn't have getUserMedia()");
					calleeReady=true;
				}
		
			
		}
	 
  	} catch (e){
  		console.log("Failed to create RTCPeerConnection, exception: " + e.message);
	    iosocket.emit("hangup",calleeId);
	    alert("There is a problem, perhaps your browser is not compatible");
	     }
	
	}   
	
	//recCandidate function receive ICE candidates from the other client and adds them to the PeerConnection object,
	//to find a suitable connection in order to establish a peerConnection.
	function recCandidate(candidate,id){
		if (id === calleeId){
			console.log("Cand receive:" + candidate.candidate);
			peerConnection.addIceCandidate(new RTCIceCandidate(candidate));
		}else{
			console.log("Droping candidate");
		}
	}
	
	 
	// Adding listener to Candidate and assigning an ID to be able to remove it later.          
	iosocket.on("candidate",recCandidate);
	
	//Same than with Candidate, but for SDP
	//recSdp function receives a sdp object from the other client with the necessary information to set the call
	// parameters, (available codecs mainly).
	function recSdp(sdp,id){
		if (id === calleeId){
			console.log("sdp Received");
			  	console.log(peerConnection);
	//		var signal = JSON.parse(sdp);
			peerConnection.setRemoteDescription(new RTCSessionDescription(sdp));
			console.log(sdp);
			if (sdp.type === "offer"){
				console.log("Creating answer");
				if (navigator.webkitRTCPeerConnection){
					peerConnection.createAnswer(successSDP,failSDP,mediaConstraints);
				}else{
					peerConnection.createAnswer(successSDP,failSDP,mozMediaConstraints);
				}
				console.log("answer created");
				
			}
			iosocket.removeListener("sdp",recSdp);
		}else{
			console.log("Droping sdp message");
			


		}
	}
	iosocket.on("sdp", recSdp);
	
	addToConference(calleeId);
	startPeerConnection(state);

}
