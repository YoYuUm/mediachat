var http= require("http"),
    port= 8080,
    url = require("url"),
    socketio = require("socket.io"),
//	util = require('util'),
	fs = require("fs"),
	formidable = require("formidable");




function start(route, handle){
    function onRequest(request,response) {
        //var postData = "";
        var pathname = url.parse(request.url).pathname;
        if (request.method === "GET"){	        
	        console.log("Request for" + pathname);

	        route(handle,pathname,response,request);
	        
	       }else if(request.method === "POST"){ //Its a POST request, asking for a file sent.
	       	console.log("POST received");
					var data = null;
	        		var form = new formidable.IncomingForm();
	        		        		
	        		form.parse(request, function(error, fields, files){
						    if(!error){
							    data = fields;
		                        //Looking for a file for the user which is requesting. "data.id"
								io.sockets.socket(data.id).get("files",function(err,files){
									
					        		if (!err){
					        		
					        			if (files[data.filename]){	
					        			
					        			
									 	 	var beg= files[data.filename][0].indexOf(":")+1;
									 	 	var end= files[data.filename][0].indexOf(";")
									 	 	
										 	var type = files[data.filename][0].substring(beg,end);
										 	var coding = files[data.filename][0].substr(end+1);
							 	
											var dataBuffer = new Buffer(files[data.filename][1], coding);
											//console.log(dataBuffer);
											
											response.setHeader('Content-type', type);
					        				response.setHeader('Content-disposition', 'attachment; filename='+data.filename);
				        					response.write(dataBuffer);
				        					response.end();
											
											//Deleting file from server after send it
											files[data.filename][0] = "";
											files[data.filename][1] = "";
					        				files[data.filename] = null;
			        					}else{
			        						console.log("There is no files with that name");
											response.end(); //TODO : Answer with something valid but without reloading the page.
			        					}			
				        		}else{
				        			console.log("error");
				        			console.log(err);
				        		}
	        				});
						}else{
							console.log("error");
							console.log(error);
						}
					
					});
									
		        	
	        	
	        	
	        	
	        	
	        	
	        	//console.log(request);
	        	
	        	
	        	
	        }
        }
        
    
    
    function onConnection(socket) {
      //Chat basic listeners  
        socket.on("nickAssign", function(nick,callback){
            
            var success = true;
            var id;
            for (id in io.sockets.manager.connected){  // Geting all the users in the chat and sending them to the client
            	io.sockets.socket(id).get("nickname",function(err,nickname){
            		if (nick === nickname){
            			success=false       				
        			}
        			if (id && nickname){
            		console.log("sending " + nickname+ "to " +nick);
            		socket.emit("join",id,nickname);
            		}
            	}) 
             
            }
            
            if (success){
            	io.sockets.emit("console", nick + " has joined in to the room");
            	io.sockets.emit("join",socket.id,nick);	 //Client announcing he is in the chat
            	socket.set("nickname", nick);          	
            }
            callback(success);
        });
        socket.on("message", function(msg){
        	socket.get("nickname", function(err,nickname){  // get socket Nickname
        		
                if (! err){
                    //extra debug
                    console.log(nickname);
                    console.log(msg);
                    //

        			io.sockets.emit("message", nickname + ": " + msg);
        			
        			//console.log(socket.handshake.address.address);   Get socket IP  	
				}	    		
        	});       
        });
        
        socket.on('disconnect', function () {
        	
        	io.sockets.emit("leave",socket.id); //Removing client from chat
                    	
      	});
        
        socket.on('poke', function(id){
        	io.sockets.socket(id).emit("console",socket.id + " is poking you! ");          	
        });
        
        //WebRTC + WebCam listeners
        
        socket.on('peer', function(id){
        	io.sockets.socket(id).emit("peer",socket.id);           	
        });
        socket.on('ready', function(id){
        	io.sockets.socket(id).emit("ready",socket.id);           	
        });
        
        socket.on('hangup', function(id){
        	console.log("--------------------------------------------------------");
        	console.log("HangUP");
        	io.sockets.socket(id).emit("hangup",socket.id);           	
        });
        
        socket.on('candidate', function(id, message){
        	io.sockets.socket(id).emit("candidate",message,socket.id);           	
        });
        
        socket.on('sdp', function(id, message){
        	io.sockets.socket(id).emit("sdp",message,socket.id);           	
        });

        // Room listeners
        
        function inviteRoom(room,clients){
            var id;
            for(var i = 0; i<=clients.length;i++){
                console.log(clients[i]);
               io.sockets.socket(clients[i]).emit("inviteroom",room,socket.id);  
            }
        }

        socket.on('createroom', function(room,clients){
            socket.join(room);
            inviteRoom(room,clients);

        });



        socket.on('inviteroom', function(room,clients){
            inviteRoom(room,clients);
        })

        socket.on('joinroom', function(room,clients){
            console.log("join to " +room);
            getMatesId(room,clients,function(roomMatesId){
                clients(roomMatesId);
                socket.join(room);
            })
            
           

        });
        
        socket.on("updateroom",function(room,clients){
            
            getMatesId(room,clients,function(roomMatesId){
                clients(roomMatesId);
            })

        })

        function getMatesId(room,clients,callback){
            var roomMates = io.sockets.clients(room);
            var roomMatesId = new Array(roomMates.length);
            for ( var i =0;i<roomMates.length ; i++){
                roomMatesId[i]= roomMates[i].id;
            }
            callback(roomMatesId)

        }

        socket.on('leaveroom',function(room){
            socket.leave(room);
            io.sockets.in(room).emit('hangup', socket.id);
        })                
        

       
       
       //Images and files listeners
       
       socket.on("pic", function(msg,id){
        	socket.get("nickname", function(err,nickname){  // get socket Nickname
        		if (! err){
        			if (id){
        				io.sockets.socket(id).emit("pic",nickname,msg,true);
					}else{
						io.sockets.emit("pic", nickname, msg);
					}	
				}	    		
        	});       
        });
        
        socket.on("file", function(msg){
        	socket.get("files", function(err,files){  // get socket Nickname
        					if (!err){
        						console.log(files);
        						if (files[msg]){
        							response.writeHead(200);
        							util.pump[files.msg,response];
        						}
        					}else{
        						console.log("something is wrong!");
        						console.log(err)
        						
        					}
        		});
        });
        
        socket.on("data", function(msg,id, callback){
        	console.log("Me llega DATAAA");
        	socket.get("nickname", function(err,nickname){  // get socket Nickname
        		if (! err){
        			if (id){
        				var vFiles = null;
        				console.log("Sending data to "+id);
        				io.sockets.socket(id).get("files", function(err,files){  // get socket Nickname
        					if (! err){
        						vFiles = files;
        					}else{
        						console.log("error geting files");
        						console.log(err);
        					}
        			//console.log(socket.handshake.address.address);   Get socket IP  	
						});
						
						if (vFiles === null){
							vFiles = new Array();
        					io.sockets.socket(id).set("files",vFiles);
        				}
        				vFiles[msg.filename] = msg.data.split(",");
        				callback(true);
        				io.sockets.socket(id).emit("data",nickname,msg.filename);
					}else{						
						console.log("Error, Data received with not receiver information");
						callback(false);
					}
							
				}else{
					console.log("Nickname Error");
					callback(false);
				}	    		
        	}); 
        });
       
       
	}
    
    
    var server = http.createServer(onRequest).listen(port);
    console.log("Servidor ejecutado");
    var io = socketio.listen(server).on("connection",onConnection);
	io.set('log level', 2);

}

exports.start = start;


		
		//var open = io.sockets.manager.open; Who is connected, some difference with connected? :S
		//var closed = io.sockets.manager.closed; Always empty :S I don't know when it contains something
		 //var clients = io.sockets.clients();
        //var handshaken = io.sockets.manager.handshaken; Here I can get the IP address