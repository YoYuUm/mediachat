var server = require("./server");
var router = require("./route");
var requestHandlers = require("./requestHandlers");

var handle = {} ;
handle["/"] = requestHandlers.start;
//handle["/chat"] = requestHandlers.chat;
//handle["/peer"] = requestHandlers.peer;
handle["/start"] = requestHandlers.start;
handle["/upload"] = requestHandlers.upload;
handle["/show"] = requestHandlers.show;
handle["serve"] = requestHandlers.serve;

server.start(router.route,handle);
