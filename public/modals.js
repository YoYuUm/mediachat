/************
*modals.js manages the modal windows that appear in the screen when user A calls to user B 
*/
function modals(){
var ringing = false;
var tempId = null;
var answer = false;
var waitTime = 30;
var showing = false;


this.stopRing = function (){
	if (ringing){
		ringing=false;
		console.log("It was ringing");
		if (showing)
			$("#ringing").modal("hide");
	}		
}

$('#ringing').on("shown",function(){
	showing=true;
	console.log("It was" + ringing);
	if (!ringing)
		$("#ringing").modal("hide");

});


this.stopCalling = function (){
	$("#calling").modal("hide");
}


this.ring = function (id, callback){
	console.log("3");
	console.log(answer);
		busy=true;
		tempId = id;
		ringtone.play();
		ringing = true;	
		$("#ringing .modal-body").text(getNickname(id)+" is calling you, Do you want to answer?");
		$("#ringing").attr("value",id);
		$("#ringing").modal();
		
		offerCall(0, function(success){
			console.log("4");
			callback(success);
		});
		
	}
	
this.calling = function (id){
		busy=true;
		tempId = id;
		$("#calling .modal-body").text("Calling "+getNickname(id)+": waiting for an answer");
		$("#calling").attr("value",id);
		$("#calling").modal();
		
	}	

$('#answerYes').click(function(){
	ringing=false;
	answer = true;
});

function onRinging(){
	
	ringtone.pause();
	if (ringing){
		console.log("Stoping call (ringing)");
		ringing=false;
		removeFromConference($('#ringing').val());
		iosocket.emit("hangup",tempId);
		
	}
	busy = false;
	showing = false;
//	$('#calling').unbind("hidden",onCalling);
}
//Subscription of 1 only use!
$('#ringing').on("hidden",onRinging);



function onCalling(){
	
	if (peers[tempId])
		if (!peers[tempId].getCalleeReady()){
			console.log("Stoping call (calling)");
			removeFromConference($('#calling').val());
			iosocket.emit("hangup",tempId);		
		}
	busy = false;
//	$('#ringing').unbind("hidden",onRinging);
	
	
}
//Subscription of 1 only use!
$('#calling').on("hidden",onCalling);


 function offerCall (i, callback){
			
			if (i === null)
				i = 0;
			if (answer){
				console.log("5");
				answer = false;				
				console.log(peerConnection);				
				showConference();
				callback(true);
			}else if ((i<waitTime) && ringing){
				i++;
				console.log("Ringing");
				setTimeout(function(){
					offerCall(i,callback)
					},1000);
			
				}else{
					
					console.log("colgando");
					modal.stopRing();
					callback(false);
					
					
				}
				
		}
		
}