var URL = window.webkitURL || window.URL;

// in dataServer.js are the 

$(function(){
	if (window.File && window.FileReader && window.FileList && window.Blob) {
  // Great success! All the File APIs are supported.
	} else {
 		alert('The File APIs are not fully supported in this browser.');
	}
//send function receives a file to send over websocket to the server, and the id of the recipient. It distinguish pictures and files.
// send uses File API
	function send(file,id){
		var type = null;
		console.log(id);
		console.log(file);
		if (file.size <= 32000000){
			if ("image" === file.type.substr(0,5)){
				type = "pic";
				toServer(file,id);
				message("Sending image "+ file.name, "blue");
			}else if(id){
				console.log("Envio DATA");
				type = "data";
				toServer(file,id);	
				message("Sending file "+ file.name, "blue");
			}else{
				alert('You can not send file "'+file.name+'" to the chat window, it is not an image.' )
			}
		}else{
			sAlert(file.name + " is too big. (Size limit 32Mb )")
		}
		
		function toServer(file,id){
			var reader = new FileReader();
				reader.onload = function (evt) {
					var data = { data : evt.target.result,
	          				filename: file.name}
	            	
	           		iosocket.emit(type,data,id, function(success){
	           			console.log("Calling back data");
	           			if (success){
	           				message(file.name+" has been sent", "blue"); //TODO: Say when it's sent
	           			}else{
	           				message("Error sending the file"+ file.name , "red");
	           			}
	           			
	           		});         
	           		          
	            };                     
				reader.readAsDataURL(file);
		}		
		
	}

/*
	function sendImage(file){
		console.log(file);
		console.log(file.type.substr(0,5));
		if ("image" === file.type.substr(0,5)){
			var reader = new FileReader();
				reader.onload = function (evt) {
					console.log("Roto12");
	       			console.log("Onload trigered");                         
	            	var data = evt.target.result; 
	           		iosocket.emit("pic",data);         
	           		console.log("Picture sent");          
	            };                     
				console.log("Roto4");
				reader.readAsDataURL(file);
				console.log("Roto5");
		}else{
			alert('The file "'+file.name+'" is not an image.' )
		}
	}
	*/
	$("#buttonSendImage").click(function(){
		
		$("#sendImageChat").click();
		
	});
	
	$("#sendImageChat").change(function(e){
		$(this.files).each(function(){
			//console.log(this);
			send(this);
			$("#sendImageChat").val("");
		})
	});
	
	$("#buttonSendFile").click(function(){
		
		$("#sendFile").click();
		
	});
	
	$("#sendFile").change(function(e){
		$(this.files).each(function(){
			//console.log(this);
			users = $('#userSelect').val();
			for (var id in users )
			send(this,users[id]);
			$("#sendFile").val("");
			
		})
	});
	
	
	//Drag&Drop picture on chat
	
	$('#chatText').on('dragenter',function(evt) {
         $("#dropZone").css("visibility","visible");        
    });
    //DropZone is a div on top of the chat, it's visible only when dragging.
	$('#dropZone').on('dragenter',function(evt) {
         $("#chatText").addClass("drag");        
    });
    
    $('#dropZone').on('dragleave',function(evt) {
         $("#chatText").removeClass("drag");  
         $(this).css("visibility","hidden");      
    });
		
	function handleFileSelect(evt) {
    	evt.stopPropagation();
    	evt.preventDefault();
		$("#dropZone").trigger("dragleave"); 
		console.log(evt);
    	var files = evt.dataTransfer.files; // FileList object.

	    // files is a FileList of File objects. List some properties.
	    var output = [];
	    for (var i = 0, f; f = files[i]; i++) {
	      send(f);
	    }
  	}
	
	  function handleDragOver(evt) {
	    evt.stopPropagation();
	    evt.preventDefault();
	    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
	
	  }
	
	  // Setup the drag and drop listeners.
	  var dropZone = document.getElementById('dropZone');
	  dropZone.addEventListener('dragover', handleDragOver, false);
	  dropZone.addEventListener('drop', handleFileSelect, false);
	  
	  
	
	

	//Event listeners for receiving files 

	//Receiving picture and drawing it in chat
	iosocket.on('pic', function(nickname,blob,private) {
			console.log("Picture received");
            
       		console.log("Onload trigered");
       		var color = "black";
       		if (private){
       			color = "red";
       		}                         
            //uri = data.replace("data:base64,", "data:image/png;base64,")
            message(nickname,color,blob.data);                               
   });
   
   iosocket.on('data', function(nickname,filename) {
			console.log("Data received");
			console.log(filename);
			message(nickname +" has sent you the file "+ filename , "blue");
           $('#incomingFiles').append($("<div></div>").addClass("files")
            											.append($("<p></p>").text(nickname+":"))
            								  			.append($('<a></a>').html('<img src="./img/file.png"></img>').addClass("file").click(function(){
            								  			
	            								  			$('<form method="post" action="./" style="display:none"><input type="hidden" name="filename" value="'+utf8_encode(filename)+'"/><input type="hidden" name="id" value="'+iosocket.socket.sessionid+'"/></form>').submit().remove();
	            								  			$(this).closest("div").remove();
            								  			
            								  				
            								  			}))
            								  			.append($("<p></p>").text(filename))
           									  );                                         
    		   
    })
    
    //Private Data
    //Private Pictures:
    
// UTF8 functions (Borrowed from php.js http://phpjs.org/ )
function utf8_encode (argString) {
  // http://kevin.vanzonneveld.net
  // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   improved by: sowberry
  // +    tweaked by: Jack
  // +   bugfixed by: Onno Marsman
  // +   improved by: Yves Sucaet
  // +   bugfixed by: Onno Marsman
  // +   bugfixed by: Ulrich
  // +   bugfixed by: Rafal Kukawski
  // +   improved by: kirilloid
  // *     example 1: utf8_encode('Kevin van Zonneveld');
  // *     returns 1: 'Kevin van Zonneveld'

  if (argString === null || typeof argString === "undefined") {
    return "";
  }

  var string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
  var utftext = '',
    start, end, stringl = 0;

  start = end = 0;
  stringl = string.length;
  for (var n = 0; n < stringl; n++) {
    var c1 = string.charCodeAt(n);
    var enc = null;

    if (c1 < 128) {
      end++;
    } else if (c1 > 127 && c1 < 2048) {
      enc = String.fromCharCode((c1 >> 6) | 192, (c1 & 63) | 128);
    } else {
      enc = String.fromCharCode((c1 >> 12) | 224, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128);
    }
    if (enc !== null) {
      if (end > start) {
        utftext += string.slice(start, end);
      }
      utftext += enc;
      start = end = n + 1;
    }
  }

  if (end > start) {
    utftext += string.slice(start, stringl);
  }

  return utftext;
}

function utf8_decode (str_data) {
  // http://kevin.vanzonneveld.net
  // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
  // +      input by: Aman Gupta
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   improved by: Norman "zEh" Fuchs
  // +   bugfixed by: hitwork
  // +   bugfixed by: Onno Marsman
  // +      input by: Brett Zamir (http://brett-zamir.me)
  // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // *     example 1: utf8_decode('Kevin van Zonneveld');
  // *     returns 1: 'Kevin van Zonneveld'
  var tmp_arr = [],
    i = 0,
    ac = 0,
    c1 = 0,
    c2 = 0,
    c3 = 0;

  str_data += '';

  while (i < str_data.length) {
    c1 = str_data.charCodeAt(i);
    if (c1 < 128) {
      tmp_arr[ac++] = String.fromCharCode(c1);
      i++;
    } else if (c1 > 191 && c1 < 224) {
      c2 = str_data.charCodeAt(i + 1);
      tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
      i += 2;
    } else {
      c2 = str_data.charCodeAt(i + 1);
      c3 = str_data.charCodeAt(i + 2);
      tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
      i += 3;
    }
  }

  return tmp_arr.join('');
}


    
});

