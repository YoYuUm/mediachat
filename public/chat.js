// Main javascript file, it manages the chat functionability 

var iosocket = io.connect(); //Websocket connection
var userMedia = false; 		 //User has allowed the access to getUserMedia()
var localStream = null;      //Local video stream 
var peers= new Array();		 //List of connected peers
var busy = false;  			 //When true: There is a model in the screen 
var conf = false;			 //When true: The user is in a conference
var chatStarted=false;		 //When true: The user has inserted his username and the chat is loaded.
var userName = null;
var ringtone = document.getElementById("ringtone");
var modal = new modals();	 
var room = new room();
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia ||
 						 navigator.mozGetUserMedia || navigator.msGetUserMedia;
window.URL = window.URL || window.webkitURL;
//Interoperability
navigator.RTCSessionDescription = navigator.RTCSessionDescription || navigator.mozRTCSessionDescription
navigator.RTCIceCanddate = navigator.RTCIceCandidate || navigator.mozRTCIceCandidate
window.RTCPeerConnection = window.RTCPeerConnection || window.webkitRTCPeerConnection || window.mozRTCPeerConnection


if (!window.WebSocket){
      alert("Your browser doesn't support WebSockets");
}

if (!navigator.getUserMedia){
      alert("Your browser doesn't support getUserMedia");
}

if (!window.RTCPeerConnection){
      alert("Your browser doesn't support RTCPeerConnection");
}


$(function(){
    
    //Get nickname from cookies    
   // $('#outgoingUserName').val(getCookie("nickName"));
    

// Basic Websocket event listeners 


 	iosocket.on('connect', function () {
        $('#incomingConsole').prepend($('<li>Connected</li>'));
   			if (chatStarted){			//If the chat where started in the connection try to reconnect with the same username.
   				maybeStart(userName);
   			}
   		});
   	
   	// Join event is triggered when some new user comes in to the chat 
   	//#id String with the id of the user
   	//#nick String with the nickname of the user
	iosocket.on('join', function(id, nick) {
			console.log(this);
			if ($('#userSelect option[value=' + id + ']').length){
					$('#userSelect option[value=' + id + ']').text(nick)
				}else{
					$('#userSelect').prepend( $('<option value="'+id+'"></option>').text(nick));
				}
		});

	// Print messages from the server in the Console
	//#message String with the text to write
	iosocket.on('console', function(message) {
	            $('#incomingConsole').prepend($('<li></li>').text(message));            
	        });
 	
 	// Event triggered when your own WebSocket lost connection with the server
    iosocket.on('disconnect', function() {
        $('#incomingConsole').prepend('<li>Disconnected</li>'); 
        $('#userSelect').html("");
    });    

    // Leave indicates when a user leave the chat room
    //#id String with the id of the user
    iosocket.on('leave',function(id){
	    		$('#userSelect option[value=' + id + ']').remove();
	    		removeFromConference(id);	    			
	});

//Chat features listeners.
	//Input box: New message to the chat 
    $('#formMessage').submit(function(event){
     event.preventDefault();
	 iosocket.emit("message",$('#outgoingChatMessage').val());
     $('#outgoingChatMessage').val('');
     });
     
    //Input box: Nickname
    $('#formNickname').submit(function(event){
     event.preventDefault();
     maybeStart($('#outgoingUserName').val())
     		
       
	});

	$("#poke").click(function() {
		users = $('#userSelect').val();
		for (var id in users )
		iosocket.emit("poke",users[id]);         		
	});
	

	//Button VideoCall: When clicked it starts a new room for a new videocall, if you are already in a videocall
	// it adds the new clients to the current call.
	$("#peerConnection").click(function() {
		var clients = $('#userSelect').val();
		if (clients){
		  	if (userMedia){
		  		if (!conf){
		  			room.createRoom($('#userSelect').val());
		  		}else{
		  			room.invite($('#userSelect').val())
		  		}
		  	}else{
		  		alert("Before calling, you must allow the browser to use your camera and video");
		  	} 
		  }else{
		  	sAlert("First select the users you want to call");
		  }   		
	});
	

	$("#hangUp").click(function(){ 
		console.log("click hung up triggered");
        room.leave();
        if (peers.length < 0){
        	for (var i in peers)
        		removeFromConference(i);
        }else{
        	
        	$(".remote").each(function(i){
        		var id= $(this).attr("id");
        		removeFromConference(id);
				iosocket.emit("hangup",id);
        	})
        }	            					
	});


	//Change the capturing devices(Mic and Camera)
	$("#updateUserMedia").click(function(event){
		event.preventDefault();
		updateUserMedia();
	});	


	/*
	$('.modal').each(function(index){
		$(this).on("show",function(){
			busy=true;
		})
		
	});
	*/

	//MaybeStart try to start the chat if the Websocket connection is established, and the nickname is not repeated, it can be
	//called after loosing the conection with the server, in that case it tries to recover back the old client's username.
	function maybeStart(nickname){
		if (iosocket.socket.connected){
				iosocket.emit("nickAssign",nickname,function(success){
					
			     	if (success){
			     		userName=$('#outgoingUserName').val() 
			            $('#outgoingUserName').val("")
			            $('#ChatMessage').fadeIn();
			            $('#UserName').fadeOut();
			            // Listen to connections or disconnections to the chat
			            
			            startChat();
			            transitionJoin();
			       }else{
			       	if (chatStarted){
			       		console.log(chatStarted)
			       		console.log("Ey you were down! trying to reconnect with the same id")
			       		setTimeout(function(){

			       			maybeStart(nickname);
			       		},5000)
			       	}else{
			       		console.log(chatStarted)
						sAlert("Nickname in use, try another one");
			       	}
			       }
		    	});
			}else{
				sAlert("It is not possible to connect to the server. Try again in a few seconds. If the error persist, please, check your Internet connection or your firewall permits.");
			}
	}
	// StartChat sets the necessary listeners for the chat
	function startChat(){

		if (!chatStarted){    	
	    	iosocket.on('message', function(msg) {
	            message(msg);
	        });
	        
	        // Peer event is triggered from the caller PeerConnection Object.
	        // It starts the local peerConnection object as callee and adds it to the peers array.
	        // #id String with the caller id.
	        iosocket.on('peer',function(id){
	        	if (!busy && userMedia){
	        		console.log("new peer");
	        		peers[id] = new pc(id,false,localStream);
	        		if (!conf){
	        			showConference();
	        		} 
	        	}else{
	        		iosocket.emit("hangup",id);
	        	}
	        	
	        });

	        // inviteroom is invoked by a room object, it starts shows a modal offering to start a conference
	        // If the user accept he will join the room
	        //#roomId String with the id of the room
	        //#calleeId String with the Caller Id.
	        iosocket.on('inviteroom', function(roomId, callerId){
	        	
	        	if (!conf && !busy && userMedia){
					modal.ring(callerId, function(success){
						if (success){
							room.joinRoom(roomId);
						}else{
							console.log("Offer declined");
						}

					});
				}else{
					iosocket.emit("hangup",callerId);
				}

	        });

	        // Ready it's invoked when the callee accept the call, it allows the peerConnection to continue
	        iosocket.on('ready',function(id){
	        	console.log("setting ready");
	        	peers[id].setCalleeReady(true); 
	        });
	      
	      	//hangup indicates that the user in the id is leaving the conference
	      	iosocket.on('hangup',function(id){
	      		console.log("HangUP !!! received from "+ getNickname(id));

	      		modal.stopRing();
	      		removeFromConference(id);
	      	});
      		
      		updateUserMedia();
      	}
	    	
		chatStarted = true
	}
    //Animation when starting the chat.  	
	function transitionJoin(){
		$("#welcome").slideUp();
		$("#content").collapse();
	}
});

// updateUserMedia function allows the user to enable his capturing devices (Mic and Camera) It uses the implementation of getUserMedia
function updateUserMedia(){
	if (!conf){ 
		var audio = document.getElementById("audio").checked;
		var video = document.getElementById("video").checked;

		if(audio || video){
			console.log("What is wrong?");
			if (navigator.getUserMedia && window.RTCPeerConnection)
		    	navigator.getUserMedia({"video":video ,"audio":audio}, function (stream){
					userMedia = true;
					localStream = stream;
					console.log("addingStream NOW")	;
				},function(err){ console.log(err)});
	    }else{
				
				userMedia=false;
	    		localStream=null;
				console.log("removing stream")	;
	    }
	}else{
		sAlert("You need to hang up before you update your media.")
	}
}

//Animation showing the conference box
function showConference(){
	if (!conf){
			conf = true;
			sAlert(null,true);
			//$("#conference").css("display","inline")
 			$("#conference").collapse("show");
			$(".lbox").css('cssText', 'height: 40% !important');
			$("#localVideo").attr("src",URL.createObjectURL(localStream));
 	}		
}

//Animation hiding the conference box
function hideConference(){
	conf = false;
	$("#conference").collapse("hide");
	$("#localVideo").attr("src","");
	$(".lbox").css('cssText', 'height: 70% !important');
	sAlert("Conference is over");
	//setTimeout(function(){$("#conference").css("display","none")},300);
	
}

function getNickname(id){
	return $("option[value="+id+"]").text();	
}

// addToConference function adds an remote video and div elements to the conference. 
// If it's called with message parameter, it doesn't add the video element and shows the message in stead.
// #id String id of the client to add to the conference
// #message String Message to show in stead of insert the video element
function addToConference(id,message){
	var html = '<p>'+getNickname(id)+'</p><video autoplay="autoplay" width="320" height="240" id="'+id+'" poster="/call.png"></video>'
	
	if (message){
		$("#conference").append($("<div></div>").attr("id",id)
											.addClass("conference")
											.addClass("remote")
											.html(message));
		}else if($("#conference #"+id).length){
			$("#conference #"+id).html(html)
			
			}else{
				$("#conference").append($("<div></div>").attr("id",id)
											.addClass("conference")
											.addClass("remote")
											.html(html));

			}									
}

// removeFromConference function removes the a client from the local conference
function removeFromConference(id){
	console.log("RemoveFromConference called");
	
	if ($("#conference #"+id).length){
		$("#conference #"+id).remove();
		if (($("#conference").children().length < 2) && conf){	
			console.log("hiding conference");
			hideConference();
		}
		if (peers[id]){
			peers[id].hangUp();
	    	peers[id]= null;
			if (conf)
			sAlert(getNickname(id) + " hung up.");
		}else{
			sAlert(getNickname(id) + " is busy.");
		}
	}
}

//message function add a message to the chat windows, it only needs message parameter.
//#message String with the message to print
//#color(opt) String with the color of the message , default = black
//#pic(opt) String in base64 with a picture data.
function message(message,color,pic){
	var top= $("#chatText").scrollTop();
	var outer= $("#chatText").outerHeight();
	var height = $("#chatText")[0].scrollHeight;
	if (!color)
		color = "black";
		
	if ((top+outer) >= height){
		appendToChat(message,color,pic);
		
		$("#chatText").scrollTop(height);		
	}else {
		sAlert("There is new messages in the chat, scroll down to see them.")
		appendToChat(message,color,pic);
	}
	
}
// appendToChat is used by message
function appendToChat(message,color,pic){	
	if (!pic){
		$('#incomingChatMessages').append($('<li style="color:'+color+'"></li>').text(message));
	}else{
		$('#incomingChatMessages').append($('<li style="color:'+color+'"></li>').text(message+": ")
            								  .append($('<img></img>').addClass("image").attr("src", pic)) 
           									  );  
	}
	
}

// sAlert shows an alert in the app
//#message String message to show in the alert
//#close(opt) Boolean , if true the alert box will disappear.
function sAlert(message,close){
	
	$("#alert").html($('<div class="alert fade in"></div>')
										.html('<button type="button" class="close" data-dismiss="alert">&times;</button>')
										.append($("<div></div>").text(message)));
	$(".alert").addClass("in");

	if (close){
		$("#alert button").click();
	}
}






//// Cookies management // W3Schools code
function setCookie(c_name,value,exdays)
{
var exdate=new Date();
exdate.setDate(exdate.getDate() + exdays);
var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
document.cookie=c_name + "=" + c_value;
}
function getCookie(c_name)
{
var i,x,y,ARRcookies=document.cookie.split(";");
for (i=0;i<ARRcookies.length;i++)
{
  x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  x=x.replace(/^\s+|\s+$/g,"");
  if (x==c_name)
    {
    return unescape(y);
    }
  }
}
