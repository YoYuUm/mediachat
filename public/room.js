
/*The room concept is needed for Video Call conference. Before a video call is started
* the participant join into the room where the rest of the call participants are.
* A user can get into the room is somebody invites him ( calling him ), and when that user
* gets into the room, he calls automatically to all the users inside that room
*/

function room(){

	var roomId = null;


// Create a room and join in to it. 
// #members is an array of Strings with the ID of the callees. 
this.createRoom = function (members){
		//busy = true;
		roomId = genId(20);
		sAlert("Starting conference...")

		addMembers(members,function(list){
			iosocket.emit("createroom", roomId , list)
		});
		
		
		//I'm waiting 500 ms to show the conference, I do this because sometimes the 
		//modal animation crashed if the callee was busy. 
		setTimeout(function(){
			if($("#conference").children().length > 1){
				showConference()
				sAlert("",true);}
				},500);
		

		// HangUp timeout
		setTimeout(function(){
			var init = false
			for(var i in peers){
				if (peers[i])
					init = true;
			}
			if (!init){
				$("#hangUp").click();
			}
		},30000)

		
	}

// Get the users in a room and call them if they are not in the conference already
function updateRoom(){
	console.log("Updating room1");
	iosocket.emit("updateroom",roomId,function(clients){
		console.log("Updating room2");
		for (var i = 0 ; i< clients.length ;i++){
			var id = clients[i];
			 if (iosocket.socket.sessionid != id){
                    if (!peers[id]){
                    	console.log("calling to " + id);
                        peers[id] = new pc(id,true,localStream);
                    }
                }
        }

	})

}


this.invite = function(members){
	addMembers(members,function(list){
		iosocket.emit("inviteroom", roomId , list);
	})

}

this.joinRoom = function (name){

	roomId = name;
	iosocket.emit("joinroom", name, function (clients){
		console.log("-----------------LIST OF CLIENTS IN THE ROOM------------------------");
		console.log(clients);
		for (var i = 0 ; i< clients.length ;i++){
			var id = clients[i];
			 if (iosocket.socket.sessionid != id){
                    if (!peers[id]){
                    	console.log("calling to " + id);
                        peers[id] = new pc(id,true,localStream);
                    }else{
                        sAlert("You are already talking with him");
                    }
                }else{
                    sAlert("You can't call yourself");
                }

        }


	})

	}

this.leave = function (){

	iosocket.emit("leaveroom", roomId);
}

// Add members of the conference to the DOM.
function addMembers (members,callback){
	for (var i = 0; i < members.length; i++){
			var id = members[i];
			if (iosocket.socket.sessionid != id){
                    if (!peers[id]){
                    	addToConference(id,'<p> Calling '+getNickname(id) + "... </p>");
                    }else{
                    	members.splice(i, 1);
                    	i--;
                        sAlert(getNickname(id) +" is already in the conference");
                    }
                }else{
                	members.splice(i, 1);
                	i--;
                    sAlert("You can't call yourself");
                }
			

		}
	if (callback)
	callback(members);
}

// ID randomizer.
function genId(length) {
		    
		    var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890", returnValue = "", x, i;

		    for (x = 0; x < length; x += 1) {
		        i = Math.floor(Math.random() * 62);
		        returnValue += chars.charAt(i);
		    }

		    return returnValue;
		}
}